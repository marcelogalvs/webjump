<?php
    include('conecta.php');

if(isset($_POST['btnSave'])){
    
    if($_GET['ref']=="novo"){
        $caminho='images/product/' . $_FILES['txtFoto']['name'];

        move_uploaded_file($_FILES['txtFoto']['tmp_name'], $caminho);

        mysqli_query($conecta, "INSERT INTO produtos (sku, nome, preco, quantidade, fkCategoria, descricao, foto) values (
        '" . $_POST['txtCodigo'] . "',
        '" . $_POST['txtNome'] . "',
        '" . $_POST['txtPreco'] . "',
        '" . $_POST['txtQuantidade'] . "',
        '" . $_POST['txtCategoria'] . "',
        '" . $_POST['txtDescricao'] . "',
        '" . $caminho . "'
        )");
        
        header("Location: products.php");
        exit;
    }
    
    if($_GET['ref']=="alt"){
        $caminho='images/product/' . $_FILES['txtFoto']['name'];

        move_uploaded_file($_FILES['txtFoto']['tmp_name'], $caminho);


        mysqli_query($conecta, "UPDATE produtos SET nome='" . $_POST['txtNome'] . "',
        preco       = '" . $_POST['txtPreco'] . "', 
        quantidade  = '" . $_POST['txtQuantidade'] . "',
        fkCategoria = '" . $_POST['txtCategoria'] . "',
        descricao   = '" . $_POST['txtDescricao'] . "',
        foto        = '" . $caminho . "'
        WHERE
        sku = '" . $_POST['txtCodigo'] . "'        
        ");
        
        header("Location: products.php");
        exit;
    }
    
}
?>

<!doctype html>
<html ⚡>

<head>
    <title>Webjump | Backend Test | Add Product</title>
    <meta charset="utf-8">

    <link rel="stylesheet" type="text/css" media="all" href="css/style.css" />
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,800" rel="stylesheet">
    <meta name="viewport" content="width=device-width,minimum-scale=1">
    <style amp-boilerplate>
        body {
            -webkit-animation: -amp-start 8s steps(1, end) 0s 1 normal both;
            -moz-animation: -amp-start 8s steps(1, end) 0s 1 normal both;
            -ms-animation: -amp-start 8s steps(1, end) 0s 1 normal both;
            animation: -amp-start 8s steps(1, end) 0s 1 normal both
        }

        @-webkit-keyframes -amp-start {
            from {
                visibility: hidden
            }

            to {
                visibility: visible
            }
        }

        @-moz-keyframes -amp-start {
            from {
                visibility: hidden
            }

            to {
                visibility: visible
            }
        }

        @-ms-keyframes -amp-start {
            from {
                visibility: hidden
            }

            to {
                visibility: visible
            }
        }

        @-o-keyframes -amp-start {
            from {
                visibility: hidden
            }

            to {
                visibility: visible
            }
        }

        @keyframes -amp-start {
            from {
                visibility: hidden
            }

            to {
                visibility: visible
            }
        }
    </style><noscript>
        <style amp-boilerplate>
            body {
                -webkit-animation: none;
                -moz-animation: none;
                -ms-animation: none;
                animation: none
            }
        </style>
    </noscript>
    <script async src="https://cdn.ampproject.org/v0.js"></script>
    <script async custom-element="amp-fit-text" src="https://cdn.ampproject.org/v0/amp-fit-text-0.1.js"></script>
    <script async custom-element="amp-sidebar" src="https://cdn.ampproject.org/v0/amp-sidebar-0.1.js"></script>
</head>
<!-- Header -->
<amp-sidebar id="sidebar" class="sample-sidebar" layout="nodisplay" side="left">
    <div class="close-menu">
        <a on="tap:sidebar.toggle">
            <img src="images/bt-close.png" alt="Close Menu" width="24" height="24" />
        </a>
    </div>
    <a href="dashboard.html"><img src="images/menu-go-jumpers.png" alt="Welcome" width="200" height="43" /></a>
    <div>
        <ul>
            <li><a href="categories.html" class="link-menu">Categorias</a></li>
            <li><a href="products.html" class="link-menu">Produtos</a></li>
        </ul>
    </div>
</amp-sidebar>
<header>
    <div class="go-menu">
        <a on="tap:sidebar.toggle">☰</a>
        <a href="dashboard.html" class="link-logo"><img src="images/go-logo.png" alt="Welcome" width="69" height="430" /></a>
    </div>
    <div class="right-box">
        <span class="go-title">Administration Panel</span>
    </div>
</header>
<!-- Header -->
<!-- Main Content -->
<main class="content">
    <h1 class="title new-item">New Product</h1>

    <?php
        if($_GET['ref']=='alt'){
            $buscaProd=mysqli_query($conecta, "SELECT * FROM produtos WHERE sku=".$_GET['id']);
            $result=mysqli_fetch_assoc($buscaProd);
        }
    ?>
    <form method="post" enctype="multipart/form-data">
        <div class="input-field">
            <label for="sku" class="label">Product SKU</label>
            <input type="text" id="sku" class="input-text" name="txtCodigo" value="<?php echo $_GET['ref']=='alt' ? $result['sku'] : '' ?>" />
        </div>
        <div class="input-field">
            <label for="name" class="label">Product Name</label>
            <input type="text" id="name" class="input-text" name='txtNome' value="<?php echo $_GET['ref']=='alt' ? $result['nome'] : ''  ?>"/>
        </div>
        <div class="input-field">
            <label for="price" class="label">Price</label>
            <input type="text" id="price" class="input-text" name="txtPreco" value="<?php echo $_GET['ref']=='alt' ? $result['preco'] : ''  ?>"/>
        </div>
        <div class="input-field">
            <label for="quantity" class="label">Quantity</label>
            <input type="text" id="quantity" class="input-text" name="txtQuantidade" value="<?php echo $_GET['ref']=='alt' ? $result['quantidade'] : ''  ?>"/>
        </div>
        <div class="input-field">
            <label for="category" class="label">Categories</label>
            <select multiple id="category" class="input-text" name="txtCategoria"> 
                <?php
                    $buscaCat=mysqli_query($conecta, "SELECT * FROM categorias ORDER BY sku");
                    while($resultCat=mysqli_fetch_assoc($buscaCat)){
                ?>
                <option value="<?php echo $resultCat['sku'] ?>" 
                    <?php 
                    if($_GET['ref']=='alt'){ 
                        echo $resultCat['sku']==$result['fkCategoria'] ? 'selected' : ''; 
                    } 
                    ?>>
                    <?php echo $resultCat['nome'] ?></option>
                <?php } ?>
            </select>
        </div>
        <div class="input-field">
            <label for="description" class="label">Description</label>
            <textarea id="description" class="input-text" name="txtDescricao"><?php echo $_GET['ref']=='alt' ? $result['descricao'] : ''  ?></textarea>
        </div>
        <div class="input-field">
            <label for="description" class="label">Foto do Produto</label>
            <input type="file" class="input-text" name="txtFoto" value="<?php echo $_GET['ref']=='alt' ? $result['foto'] : ''  ?>"/>
        </div>
        <div class="actions-form">
            <a href="products.php" class="action back">Back</a>
            <input class="btn-submit btn-action" type="submit" value="Save Product" name="btnSave" />
        </div>

    </form>
</main>
<!-- Main Content -->

<!-- Footer -->
<footer>
    <div class="footer-image">
        <img src="images/go-jumpers.png" width="119" height="26" alt="Go Jumpers" />
    </div>
    <div class="email-content">
        <span>go@jumpers.com.br</span>
    </div>
</footer>
<!-- Footer -->
</body>

</html>